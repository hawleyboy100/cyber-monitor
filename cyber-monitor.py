from DiscordHooks import Hook
from colorama import init, Fore, Back, Style
import time
import requests
from datetime import datetime


webhook = 'https://discordapp.com/api/webhooks/446895690363437066/l4dgzjRpnma6iBTUGVyNQzxApKzrfLl0mM7-sPxg6HK7GpbT-5Fb8-vBlJ1-dChUz78y'
cyberurl = 'https://www.cybersole.io/'
init(convert=True)

def cyberpost():
    Hook(hook_url=webhook, username="Bot Restock",
         avatar_url='https://regmedia.co.uk/2016/01/12/slack.jpg?x=1200&y=794',
         content="CyberAIO Restock! https://www.cybersole.io/ \U0001f62e").execute()
    return 0


def check():
    with requests.session() as c:
            page1 = c.get(cyberurl)
            print(Fore.YELLOW + 'Waiting to refresh')
            time.sleep(15)
            page2 = c.get(cyberurl)
            if page1.content == page2.content:
                print(Fore.RED + '[-]No Change Detected on ' + str(cyberurl) + "\n" + str(datetime.now()), Style.RESET_ALL)
                check()
            else:
                status_string = 'Change Detected at ' + str(cyberurl) + "\n" + str(datetime.now())
                print(Fore.GREEN + "[+]" + status_string + Style.RESET_ALL)
                cyberpost()
                print(Fore.GREEN + '\n[+]Retrieving new base page and restarting\n' + Style.RESET_ALL)
                check()


if __name__ == '__main__':
    print(Fore.MAGENTA + 'Welcome To OMonitor-cyber!' + Style.RESET_ALL)
    check()
