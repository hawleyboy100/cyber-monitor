from DiscordHooks import Hook
from termcolor import colored
import time
import requests
from datetime import datetime

webhook = 'https://discordapp.com/api/webhooks/446895690363437066/l4dgzjRpnma6iBTUGVyNQzxApKzrfLl0mM7-sPxg6HK7GpbT-5Fb8-vBlJ1-dChUz78y'
cyberurl = 'https://www.cybersole.io/'


def cyberpost():
    Hook(hook_url=webhook, username="Bot Restock",
         avatar_url='https://regmedia.co.uk/2016/01/12/slack.jpg?x=1200&y=794',
         content="CyberAIO Restock! https://www.cybersole.io/ \U0001f62e").execute()
    return 0


status_string = 'Change Detected at ' + str(cyberurl) + "\n" + str(datetime.now())
print(colored("[+]" + status_string, 'green'))
cyberpost()
print(colored('\n[+]Retrieving new base page and restarting\n', 'yellow'))
